#!/bin/sh
sudo apt install fonts-powerline
# sudo dnf install powerline-fonts
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update && sudo apt install yarn
sudo npm install -g react-native-cli
git clone https://gitlab.com/EetuHakkinen/gitautomation.git ~/projects